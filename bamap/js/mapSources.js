/*定义百度投影，这是实现无偏移加载百度地图离线瓦片核心所在。
 网上很多相关资料在用OpenLayers加载百度地图离线瓦片时都认为投影就是EPSG:3857(也就是Web墨卡托投影)。
 事实上这是错误的，因此无法做到无偏移加载。
 百度地图有自己独特的投影体系，必须在OpenLayers中自定义百度投影，才能实现无偏移加载。
 百度投影实现的核心文件为bd09.js，在迈高图官网可以找到查看这个文件。*/
var projBD09 = new ol.proj.Projection({
    code: 'BD:09',
    extent : [-20037726.37,-11708041.66,20037726.37,12474104.17],
    units: 'm',
    axisOrientation: 'neu',
    global: false
});

ol.proj.addProjection(projBD09);
ol.proj.addCoordinateTransforms("EPSG:4326", "BD:09",
    function (coordinate) {
        return lngLatToMercator(coordinate);
    },
    function (coordinate) {
        return mercatorToLngLat(coordinate);
    }
);
/*定义百度地图分辨率与瓦片网格*/
var resolutions = [];
for (var i = 1; i <= 19; i++) {
    resolutions[i] = Math.pow(2, 18 - i);
}
var tilegrid = new ol.tilegrid.TileGrid({
    origin: [0, 0],
    resolutions: resolutions
});

/*加载百度地图离线瓦片不能用ol.source.XYZ，ol.source.XYZ针对谷歌地图（注意：是谷歌地图）而设计，
    而百度地图与谷歌地图使用了不同的投影、分辨率和瓦片网格。因此这里使用ol.source.TileImage来自行指定
    投影、分辨率、瓦片网格。*/

var MAP_BAIDU_SOUR =new ol.source.XYZ({
    wrapX: false,
    projection: ol.proj.get("EPSG:3857"),
    tileUrlFunction: function (tileCoord, pixelRatio, proj) {
        
        if (!tileCoord) {
            return "";
        }
        var z = "L" +(tileCoord[0]);
        //var halfTileNum = Math.pow(2, z - 1);  计算当前层级下瓦片总数的一半，用于定位整个地图的中心点
        var x = tileCoord[1] - Math.pow(2, tileCoord[0] - 1);
        var y = tileCoord[2] ;
        x1x = x;
        y1y = y;
        //位数是行R8位数，列C8位数
        var f = "C" + parseInt(x * Math.pow(2, 19 - tileCoord[0]) / 512) + "R" + parseInt(y * Math.pow(2, 19 - tileCoord[0]) / 512);

        // return  'http://192.168.0.100/mapServer/'+ z + "/" + x + "/" + y +".jpg"
        return 'http://119.159.44.184:42006/mapServer/'+f+"/"+ z + "/" + x + "," + y +".png";
    },
    attributions: ['Tiles © Luculent','<a href=>官网</a>']
});

var MAP_BAIDU_YY_SOUR = new ol.source.XYZ({
    wrapX: false,
    projection: ol.proj.get("EPSG:3857"),
    tileUrlFunction: function (tileCoord, pixelRatio, proj) {
        
        if (!tileCoord) {
            return "";
        }
        var z = "L" +(tileCoord[0]);
        //var halfTileNum = Math.pow(2, z - 1);  计算当前层级下瓦片总数的一半，用于定位整个地图的中心点
        var x = tileCoord[1] - Math.pow(2, tileCoord[0] - 1);
        var y = tileCoord[2] ;
        x1x = x;
        y1y = y;
        //位数是行R8位数，列C8位数
        var f = "C" + parseInt(x * Math.pow(2, 19 - tileCoord[0]) / 512) + "R" + parseInt(y * Math.pow(2, 19 - tileCoord[0]) / 512);
        //console.log('http://219.159.44.184:42006/mapServer/'+f+"/"+ z + "/" + x + "," + y +".png")

        // return  'http://192.168.0.100/mapServer/'+ z + "/" + x + "/" + y +".jpg"
        return 'http://119.159.44.184:42006/mapServer/'+f+"/"+ z + "/" + x + "," + y +".png";
    }
});
 