//定义仿射变换函数
var matrix = new Map();
//初始化每个底图的仿射函数
matrixData.forEach(function(item,key){
    if(item.length > 0){
        matrix.set(key,new MatrixCalc(item));
    }
});
/*
if(data instanceof Array){
    if(data.length > 0){
        matrix = new MatrixCalc(data);
    }
}*/
function affineTransform(riskId,point) {
    if (!matrix.get(riskId)) return  point;
    return  matrix.get(riskId).to_target(point[0],point[1]);
}

function affineTransTo(riskId,point) {
    if (!matrix.get(riskId)) return  point;
    return  matrix.get(riskId).to_orgin(point[0],point[1]);
}

var minZoom = 16;

var center = gcoord.transform([lat,lng],gcoord.BD09,gcoord.BD09Meter)
// 计算静态图映射到地图上的范围，图片像素为 550*344，保持比例的情况下，把分辨率放大一些
var extent = [center[0]- imgW/2, center[1]-imgH/2, center[0]+imgW/2, center[1]+imgH/2];
var options = {
    // 地图中心点
    center: center,
    // 地图显示级别
    extent: extent,
    imgW:imgW,
    imgH:imgH,
    imgUrl:imgUrl,
}
//百度在线瓦片
var resolutions = [];
for (var i = 1; i <= 19; i++) {
    resolutions[i] = Math.pow(2, 18 - i);
}
var tilegrid = new ol.tilegrid.TileGrid({
    origin: [0, 0],
    resolutions: resolutions
});

var MAP_BAIDU = new ol.source.TileImage({
    wrapX: false,
    projection: "BD:09",
    tileGrid: tilegrid,
    tileUrlFunction: function (tileCoord, pixelRatio, proj) {
        if (!tileCoord) {
            return "";
        }
        let z = tileCoord[0];
        let x = tileCoord[1];
        let y = -tileCoord[2]-1;
        if(x<0)  x = "M"+(-x);
        if(y<0) y = "M"+(-y);
        // return  'http://192.168.0.100/mapServer/'+ z + "/" + x + "/" + y +".jpg"
        return 'http://maponline2.bdimg.com/tile/?qt=vtile&x='+x+'&y='+y+'&z='+z+'&styles=pl&scaler=2&udt=20220426&from=jsapi3_0'
    }
});
var mapBdLayer = new ol.layer.Tile({
    source: MAP_BAIDU
});
//图片
var imgLayer =new ol.layer.Image({
    source: new ol.source.ImageStatic({
        wrapX: false,
        url: imgUrl,//'http://localhost:8080/images/risk/four1.png',//这里添加静态图片的地址
        projection: ol.proj.get("BD:09"),
        imageExtent: extent
    })
})

//加载坐标点
var pointSource = new ol.source.Vector({
    wrapX:false,
    projection: 'BD:09'});

//画的点
var pointLayer = new ol.layer.Vector({
    projection: 'BD:09',
    source:pointSource,
});

//这个是为了在地图上图形
var modifySource = new ol.source.Vector({
    wrapX:false,

});
var modifyVector = new ol.layer.Vector({
    source:modifySource,
    projection: 'BD:09',
    zIndex:999
});

//隐藏
imgLayer.setVisible(false);
mapBdLayer.setVisible(true);


var map = new ol.Map({
    layers: [
        imgLayer,mapBdLayer,modifyVector,pointLayer
    ],
    target: 'map',
    view: new ol.View({
        center: center,
        //瓦片加载显示的坐标系
        projection: 'BD:09',
        zoom: 15,
        maxZoom: 21,
        minZoom: 1,
    })
});

//右上角展示坐标
var c = new ol.control.MousePosition({
    projection: 'BD:09',
    targetID: "location", //默认右上角
    coordinateFormat: function(coordinate) {
        //地图上的是bdmaster要转成 bd拾取的坐标
        return ol.coordinate.format(gcoord.transform(coordinate,gcoord.BD09Meter,gcoord.BD09), '{x},{y}', 4);
    },
    pix: 6
})
map.addControl(c);

//----------加载点
function addPoints(options){
    var features=[];
    features.push(dealPoint(options));
    if(options.type == 'static'){
        pointSource.addFeatures(features);
    }else {
        modifySource.addFeatures(features);
    }
}
function dealFeature(polygon,lb,id,color){
    var feature = new ol.Feature({
        type : lb,
        geometry : polygon, // 目标坐标系
        id : id
    });
    !color ? (color = 'rgba(231,220,52,0.32)') : color;
    feature.setStyle(new ol.style.Style({
        //界面填充颜色
        fill: new ol.style.Fill({
            color: color
        }),
        //边框颜色
        stroke: new ol.style.Stroke({
            color: 'rgba(255,242,0,0.5)',
            width: 3
        }),
        //图片颜色
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: 'red'
            })
        })
    }));
    return feature;
}
function dealPoint(options){
    var point = [ (Number(options.lat)),(Number(options.lng))];
    if(!options.affine){
        point = affineTransform(options.riskId,point);
    }
    var startMarker = new ol.Feature({
        type : options.lb,
        geometry : new ol.geom.Point(gcoord.transform(
            point,  // 经纬度坐标
            gcoord.BD09  ,             // 当前坐标系
            gcoord.BD09Meter  // 目标坐标系
        )),
        id : options.id
    });

    startMarker.setStyle( new ol.style.Style({
        image : new ol.style.Icon({
            anchor : [ 0.7, 0.7 ],
            src : options.icon,
        }),
        text: new ol.style.Text({
            // 字体与大小
            font: '16px Microsoft YaHei',
            //文字填充色
            fill: new ol.style.Fill({
                color: 'rgb(17,17,17)',
            }),
            //文字边界宽度与颜色
            stroke: new ol.style.Stroke({
                color: 'rgb(255,255,255)',
                width: 1
            }),
            // 显示文本，数字需要转换为文本string类型！
            text: options.name,
            offsetY: -40,
            offsetX: -6
        })

    }));
   /* jsPlumb.ready(function () {
        jsPlumb.connect({
            source: 'save_btn',
            target: 'map',
            endpoint: 'Dot'
        })
    })*/
    return startMarker;
}

// --- 加载热区
function addPolygon(options){
    var areaFeatures = [];
    //处理区域
    var dealPolygons = dealPolygon(options.riskId,options.pointLines);
    if(!dealPolygons){
        return;
    }
    var polygon = new ol.geom.Polygon([dealPolygons]);
    areaFeatures.push(dealFeature(polygon,options.lb,options.id,options.color));
    modifySource.addFeatures(areaFeatures);
    modifyVector.setZIndex(0);
}
function dealPolygon(riskId,pointLines){
    if(!pointLines){
        return ;
    }
    var pointLines = pointLines.slice(9).slice(0,-2);
    var piontArray = pointLines.split(',');
    var pointline = [];
    piontArray.forEach(function(point) {
        var points = point.split(' ');
        //console.log(points);
        points = affineTransform(riskId,[ (Number(points[0])),(Number(points[1]))]);
        // console.log(points);
        var points = gcoord.transform(points,gcoord.BD09,gcoord.BD09Meter);
        //pointline += points[0]+' '+ points[1]+',';
        var tmpPoint = [];
        tmpPoint.push(Number(points[0]));
        tmpPoint.push(Number(points[1]));
        pointline.push(tmpPoint);
    });
    return pointline;
}

/*function affineTransform(point) {
    if (from.length != to.length) return;
    if (from.length == 0 || to.length == 0) return point;
    var X = [];
    var Y = [];
    var I = [];
    var U = [];
    var V = [];
    from.forEach((item, index) => {
        X.push(Number(item[0]));
        Y.push(Number(item[1]));
        I.push(1);
        U.push([Number(to[index][0])]);
        V.push([Number(to[index][1])]);
    })
    var XYIt = [X, Y, I];
    var resultINV = math.inv(math.multiply(XYIt, math.transpose(XYIt)))
    var resultMulti = math.multiply(resultINV, XYIt);
    var vec1 = math.multiply(resultMulti, U)
    var vec2 = math.multiply(resultMulti, V)
    return [vec1[0][0] * point[0] + point[1] * vec1[1][0] + vec1[2][0], vec2[0][0] * point[0] + point[1] * vec2[1][0] + vec2[2][0]]
}*/