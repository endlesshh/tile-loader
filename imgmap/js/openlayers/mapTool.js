
//需要的依赖库
document.write('<script src="/js/openlayers/math.js" charSet="utf-8"  ></sc' + 'ript>');
document.write('<script src="/js/openlayers/ol.js" charSet="utf-8" ></sc' + 'ript>');
document.write('<script src="/js/openlayers/bd09.js" charSet="utf-8"  ></sc' + 'ript>');
document.write('<script src="/js/openlayers/gcoord.js" charSet="utf-8"  ></sc' + 'ript>');
document.write('<script src="/js/openlayers/mapSources.js" charSet="utf-8"  ></sc' + 'ript>');
document.write('<script src="/js/openlayers/sylvester.min.js" charSet="utf-8"  ></sc' + 'ript>');
document.write('<script src="/js/openlayers/MatrixCalc.js" charSet="utf-8"  ></sc' + 'ript>');

(function(w){
    //工厂
    function jMap(options){
        return new jMap.fn.init(options);
    }
    //1.jQuery原型替换，2.给原型提供一个简写方式
    jMap.fn = jMap.prototype = {
        //下面的属性和方法是工用的，想单独用可以在各自的实例中单独声明
        constructor : jMap,
        version : '1.0.0',
        //地图
        map:{},
        //方式函数
        matrix: {},
        //最小放缩
        minZoom: 16,
        //中心点坐标
        lat: 110.224274,
        lng: 39.281935,
        //需要初始化仿射点
        matrixData: {},
        //图片的 宽高路径
        imgH: 0,
        imgW: 0,
        imgUrl: 0,
        //百度图层
        mapBdLayer:{},
        //图片图层
        imgLayer:{},
        //地图画点的
        pointSource:{},
        pointLayer:{},
        //地图上修改点的
        modifySource:{},
        modifyVector :{},
        //绘画和编辑工具
        draw:{},
        modify:{},
        /**
         *
         * 初始化仿射函数
         */
        initMatrix(matrixData){
            this.matrixData = matrixData || new Map();
            this.matrix = this.matrix || new Map();
            let that = this;
            this.matrixData.forEach(function (item, key) {
                if (item.length > 0) {
                    that.matrix.set(key, new MatrixCalc(item));
                }
            });
        },
        /**
         * 仿射变换到校准后的点
         * @param riskId
         * @param point
         * @returns {[*, *]|*}
         */
        affineTransform(riskId, point) {
            if (!this.matrix.get(riskId)) return point;
            return this.matrix.get(riskId).to_target(point[0], point[1]);
        },
        /**
         * 校准后的点恢复原始的点
         * @param riskId
         * @param point
         * @returns {[*, *]|*}
         */
        affineTransTo(riskId, point) {
            if (!this.matrix.get(riskId)) return point;
            return this.matrix.get(riskId).to_orgin(point[0], point[1]);
        },
        /**
         *
         *  lat:    经度
         *  lng:    纬度
         *  name:   名称
         *  icon:   图标
         *  id:     唯一标识
         *  lb:     点的类别 （'risk_area_point','risk_point'）,
         *  affine: 是否仿射变换 true 不变换
         *  type:   static 不能编辑
         *
         * @param options
         */
        addPoints(options) {
            let features = [];
            features.push(this.dealPoint(options));
            if (options.type == 'static') {
                this.pointSource.addFeatures(features);
            } else {
                this.modifySource.addFeatures(features);
            }
        },

        dealFeature(polygon, lb, id, color) {
            let feature = new ol.Feature({
                type: lb,
                geometry: polygon, // 目标坐标系
                id: id
            });
            !color ? (color = 'rgba(231,220,52,0.32)') : color;
            feature.setStyle(new ol.style.Style({
                //界面填充颜色
                fill: new ol.style.Fill({
                    color: color
                }),
                //边框颜色
                stroke: new ol.style.Stroke({
                    color: 'rgba(255,242,0,0.5)',
                    width: 3
                }),
                //图片颜色
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: 'red'
                    })
                })
            }));
            return feature;
        },
        dealPoint(options) {
            let point = [(Number(options.lat)), (Number(options.lng))];
            if (!options.affine) {
                point = this.affineTransform(options.riskId, point);
            }
            let startMarker = new ol.Feature({
                type: options.lb,
                geometry: new ol.geom.Point(gcoord.transform(
                    point,  // 经纬度坐标
                    gcoord.BD09,             // 当前坐标系
                    gcoord.BD09Meter  // 目标坐标系
                )),
                id: options.id
            });

            startMarker.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [0.7, 0.7],
                    src: options.icon,
                }),
                text: new ol.style.Text({
                    // 字体与大小
                    font: '16px Microsoft YaHei',
                    //文字填充色
                    fill: new ol.style.Fill({
                        color: 'rgb(17,17,17)',
                    }),
                    //文字边界宽度与颜色
                    stroke: new ol.style.Stroke({
                        color: 'rgb(255,255,255)',
                        width: 1
                    }),
                    // 显示文本，数字需要转换为文本string类型！
                    text: options.name,
                    offsetY: -40,
                    offsetX: -6
                })

            }));
            return startMarker;
        },

        // --- 加载热区
        addPolygon(options) {
            let areaFeatures = [];
            //处理区域
            let dealPolygons = this.dealPolygon(options.riskId, options.pointLines);
            if (!dealPolygons) {
                return;
            }
            let polygon = new ol.geom.Polygon([dealPolygons]);
            areaFeatures.push(this.dealFeature(polygon, options.lb, options.id, options.color));
            this.modifySource.addFeatures(areaFeatures);
            this.modifyVector.setZIndex(0);
        },

        dealPolygon(riskId, pointLines) {
            if (!pointLines) {
                return;
            }
            let pointline = [];
            let that = this;
            pointLines.slice(9).slice(0, -2).split(',').forEach(function (point) {
                let points = that.affineTransform(riskId, point.split(' '));
                points = gcoord.transform(points, gcoord.BD09, gcoord.BD09Meter);
                pointline.push([Number(points[0]),Number(points[1])]);
            });
            return pointline;
        },

        initDrawPoint(){

            this.draw = new ol.interaction.Draw({
                source: this.modifySource,// 注意设置source，这样绘制好的线，就会添加到这个source里
                type: 'Point',// (typeSelect.value)
                projection: 'BD:09',
            });
            this.map.addInteraction(this.draw);

        },

        initDrawPolygon(){
            this.draw = new ol.interaction.Draw({
                source:mapTools.modifySource,// 注意设置source，这样绘制好的线，就会添加到这个source里
                type: 'Polygon',// (typeSelect.value)
                projection: 'BD:09',
            });
            this.map.addInteraction(this.draw);
        },

        initModify(){
            this.modify = new ol.interaction.Modify({
                source: this.modifySource,
                // features: select.getFeatures(),
            });
            let snap = new ol.interaction.Snap({
                source: this.modifySource
            });
            this.map.addInteraction(snap);
            this.map.addInteraction(this.modify);


        },
        //地图飞行事件
        flyTo(location, done,view,riskId) {

            if(riskId){
                location = this.affineTransform(riskId,location);
                location = gcoord.transform(location,gcoord.BD09,gcoord.BD09Meter);
            }

            let duration = 3000;
            let zoom = view.getZoom();
            let parts = 2;
            let called = false;
            this.imgLayer.setVisible(true);
            this.mapBdLayer.setVisible(true);
            function callback(complete) {
                --parts;
                if (called) {
                    return;
                }
                if (parts === 0 || !complete) {
                    called = true;
                    done(complete);
                }
            }
            view.animate({
                center: location,
                duration: duration
            }, callback);
            view.animate({
                zoom: zoom - 1,
                duration: duration / 2
            }, {
                zoom: zoom,
                duration: duration / 2
            }, callback);
        },
        //----------------下面是基础点和校验点的操作------------
        //不动的点
        basePoints:[],
        //编辑的点
        editPoints:[],
        //校验点和基准点保存
        pointsMap:new Map(),
        //基础点进行初始化
        benchmarkPoints(action){

            let that = this;
            function initAreaMaps(point,index){
                let base1 = point.split(",");
                let options = {
                    lat: base1[0],
                    lng: base1[1],
                    name:  '第'+(index+1)+'个基点',
                    icon: '/images/portal/red_rico.png',
                    id: index,
                    lb: 'risk_area_point',
                    affine:true
                }
                that.addPoints(options);
                that.pointsMap.set(index,options);
            }
            function initBasePoints(point,index){
                let base1 = point.split(",");
                let options = {
                    lat:  base1[0],
                    lng: base1[1],
                    name: '第'+(index+1)+'个基点',
                    icon: '/images/portal/blue_rico.png',
                    id: index,
                    lb: 'risk_img',
                    type: 'static',
                    affine:true
                }
                that.addPoints(options);
            }

            that.editPoints.forEach((item, index) => {
                initAreaMaps(item, index);
            });
            if(action == 'bench'){
                //初始化不动的点
                that.basePoints.forEach((item, index) => {
                    initBasePoints(item, index);
                });
            }
        },
        /**
         * 添加基础点
         */
        benchmarkDraw(){
            let that = this;
            this.draw.on('drawend', function (evt) {

                let zoom = that.map.getView().getZoom();
                //地图放缩
                if(zoom <= that.minZoom){
                    lay.error("地图放缩过小，请放大地图");
                }else {
                    if(that.pointsMap.size >= 6){
                        lay.error("只能标准6个基准点");
                        that.modifySource.clear();
                        that.pointsMap.forEach(function(value,key){
                            var options = {
                                lat: value.lat,
                                lng: value.lng,
                                name: value.name,
                                icon: '/images/portal/red_rico.png',
                                id: value.id,
                                lb: 'risk_area_point',
                                affine:true,
                            }
                            that.addPoints(options);
                        });
                    }else {
                        let feature=evt.feature;
                        console.log("-----通过gcoord转换坐标--------");
                        let points = feature.getGeometry().getCoordinates();
                        points = gcoord.transform(points,gcoord.BD09Meter,gcoord.BD09);
                        let hot = {};
                        hot.lat = points[0].toFixed(6);
                        hot.lng = points[1].toFixed(6);
                        hot.id = that.pointsMap.size;
                        hot.name = '第'+( that.pointsMap.size + 1)+'个基点';
                        that.pointsMap.set(hot.id,hot);
                        let options = {
                            lat: hot.lat,
                            lng: hot.lng,
                            name: hot.name,
                            icon: '/images/portal/red_rico.png',
                            id: hot.id,
                            lb: 'risk_area_point',
                            affine:true,
                        }
                        that.addPoints(options);
                    }
                }
            })
        },
        benchmarkModify(){
            let that = this;
            this.modify.on('modifyend', function (evt) {

                let zoom = that.map.getView().getZoom();
                //地图放缩
                if(zoom <= that.minZoom){
                    lay.error("地图放缩过小，请放大地图");
                }else {
                    let feature=evt.features.item(0);
                    let points = feature.getGeometry().getCoordinates();
                    console.log("-----坐标编辑--------",points,feature.getProperties()["id"]);
                    points = gcoord.transform(points,gcoord.BD09Meter,gcoord.BD09);
                    if(feature.getProperties()["id"] || feature.getProperties()["id"] == 0){
                        let hot = {};
                        hot.lat = points[0].toFixed(6);
                        hot.lng = points[1].toFixed(6);
                        hot.id = feature.getProperties()["id"];
                        hot.name = '第'+(hot.id+ 1)+'个点';
                        console.log(that.pointsMap)
                        that.pointsMap.set(hot.id,hot);
                        that.modifySource.clear();
                        that.pointsMap.forEach(function(value,key){
                            var options = {
                                lat: value.lat,
                                lng: value.lng,
                                name: value.name,
                                icon: '/images/portal/red_rico.png',
                                id: value.id,
                                lb: 'risk_area_point',
                                affine:true,
                            }
                            that.addPoints(options);
                        });
                    }
                }
            })
        },
//----------------下面是画风险点区域-------------------
        pointsAreaData:{},
        riskPointDraw(riskId){
            let that = this;
            this.draw.on('drawend', function (evt) {

                let zoom = that.map.getView().getZoom();
                //地图放缩
                if(zoom <= that.minZoom){
                    lay.error("地图放缩过小，请放大地图");
                    return;
                }
                let feature=evt.feature;
                console.log("-----通过gcoord转换坐标--------");
                that.setResultPoints(feature,riskId);
            });
        },
    };
    //给jMap的构造函数和原型对象，都添加一个extend方法，该方法用来拓展对象功能
    //  1：jMap.extend() 的调用并不会把方法扩展到对象的实例上，引用它的方法也需要通过jQuery类来实现
    //  2：jMap.fn.extend()的调用把方法扩展到了对象的prototype上，所以实例化一个jQuery对象的时候，它就具有了这些方法
    jMap.extend = jMap.fn.extend = function(obj){
        for(var key in obj){
            this[key] = obj[key];
        }
    };
    //给jMap添加静态方法
    jMap.fn.extend({
        riskPointModify:function(riskId){
            let that = this;
            this.modify.on('modifyend', function (evt) {
                let feature=evt.features.item(0);
                that.setResultPoints(feature,riskId);
            });
        },
        /**
         * 设置返回的点和区域
         * @param feature
         * @param riskId
         */
        setResultPoints:function(feature,riskId){
            //中心点坐标
            let xy = feature.getGeometry().getInteriorPoint().getCoordinates();
            //画的区域
            let ct = this.affineTransTo(riskId,gcoord.transform([xy[0],xy[1]],gcoord.BD09Meter,gcoord.BD09));
            this.pointsAreaData.polygon = this.web2Bd(feature);
            this.pointsAreaData.lat = ct[0];
            this.pointsAreaData.lng = ct[1];
        },
        //进行坐标的转换
        web2Bd: function(feature){
            let pointline = [];
            let that = this;
            feature.getGeometry().getCoordinates().forEach(function(points) {
                points.forEach(function(point) {
                    point = gcoord.transform(point,gcoord.BD09Meter,gcoord.BD09);
                    point = that.affineTransTo(riskId,point);
                    pointline.push(point.join(' '));
                })
            });
            if(pointline.length > 0){
                let lines = "POLYGON(("+pointline.join(',')+"))";
                return jMap.polygon2Wkt(lines);
            }else {
                return '';
            }
        },
    });

    //给jMap添加静态方法
    jMap.extend({
        /**
         *
         * @param lines
         * @returns {*}
         */
        polygon2Wkt:function(lines){
            //geoJson数据					//writeGeometryObject
            let gson = new ol.format.GeoJSON().writeGeometry(new ol.format.WKT().readGeometry(lines));
            //geoJson转WKT数据
            return new ol.format.WKT().writeGeometry(new ol.format.GeoJSON().readGeometry(gson));
        },

    });



    // 初始化地图
    var init = jMap.fn.init = function (options) {
        this.lat = options.lat;
        this.lng = options.lng;
        this.imgW = options.imgW;
        this.imgH = options.imgH;
        this.imgUrl = options.imgUrl;
        this.matrixData = options.matrixData || new Map();
        this.matrix = new Map();
        let that = this;
        this.matrixData.forEach(function (item, key) {
            if (item.length > 0) {
                that.matrix.set(key, new MatrixCalc(item));
            }
        });
        //需要初始化
        let center = gcoord.transform([this.lat, this.lng], gcoord.BD09, gcoord.BD09Meter);
        // 计算静态图映射到地图上的范围，图片像素为 550*344，保持比例的情况下，把分辨率放大一些
        let extent = [center[0] - this.imgW / 2, center[1] - this.imgH / 2, center[0] + this.imgW / 2, center[1] + this.imgH / 2];

        //百度在线瓦片放缩栅格
        let resolutions = [];
        Array.from({length: 19}, (v, i) => resolutions[++i] = Math.pow(2, 18 - i));

        let tilegrid = new ol.tilegrid.TileGrid({
            origin: [0, 0],
            resolutions: resolutions
        });
        let MAP_BAIDU = new ol.source.TileImage({
            wrapX: false,
            projection: "BD:09",
            tileGrid: tilegrid,
            tileUrlFunction: function (tileCoord, pixelRatio, proj) {
                if (!tileCoord) {
                    return "";
                }
                let z = tileCoord[0];
                let x = tileCoord[1];
                let y = -tileCoord[2] - 1;
                if (x < 0) x = "M" + (-x);
                if (y < 0) y = "M" + (-y);
                // return  'http://192.168.0.100/mapServer/'+ z + "/" + x + "/" + y +".jpg"
                return 'http://maponline2.bdimg.com/tile/?qt=vtile&x=' + x + '&y=' + y + '&z=' + z + '&styles=pl&scaler=2&udt=20220426&from=jsapi3_0'
            }
        });
        this.mapBdLayer = new ol.layer.Tile({
            source: MAP_BAIDU
        });
        //图片
        this.imgLayer = new ol.layer.Image({
            source: new ol.source.ImageStatic({
                wrapX: false,
                url: imgUrl,//'http://localhost:8080/images/risk/four1.png',//这里添加静态图片的地址
                projection: ol.proj.get("BD:09"),
                imageExtent: extent
            })
        });
        //加载坐标点
        this.pointSource = new ol.source.Vector({
            wrapX: false,
            projection: 'BD:09'
        });

        //画的点
        this.pointLayer = new ol.layer.Vector({
            projection: 'BD:09',
            source: this.pointSource,
        });

        //这个是为了在地图上图形
        this.modifySource = new ol.source.Vector({
            wrapX: false,

        });
        this.modifyVector = new ol.layer.Vector({
            source: this.modifySource,
            projection: 'BD:09',
            zIndex: 999
        });
        //隐藏
        this.imgLayer.setVisible(false);
        this.mapBdLayer.setVisible(true);
        this.map = new ol.Map({
            layers: [
                this.imgLayer, this.mapBdLayer, this.modifyVector, this.pointLayer
            ],
            target: options.map,
            view: new ol.View({
                center: center,
                //瓦片加载显示的坐标系
                projection: 'BD:09',
                zoom: 15,
                maxZoom: 21,
                minZoom: 1,
            })
        });

        //右上角展示坐标
        let c = new ol.control.MousePosition({
            projection: 'BD:09',
            targetID: "location", //默认右上角
            coordinateFormat: function (coordinate) {
                //地图上的是bdmaster要转成 bd拾取的坐标
                return ol.coordinate.format(gcoord.transform(coordinate, gcoord.BD09Meter, gcoord.BD09), '{x},{y}', 4);
            },
            pix: 6
        })
        this.map.addControl(c);
        return this;
    };

    //把构造函数的原型，替换为jQuery工厂的原型
    //这么做的目的是为了实现jQuery的插件机制，让外界可以通过jQuery方便的进行扩展
    init.prototype = jMap.fn;
    w.jMap = jMap;
})(window)