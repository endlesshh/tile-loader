import com.google.common.collect.Lists;
import com.lhq.loader.bean.DownFile;
import com.lhq.loader.bean.LngLat;
import com.lhq.loader.bean.Tile;
import com.lhq.loader.commons.toolkit.CoordinateToolkit;
import com.lhq.loader.controller.vo.DownloadParamVO;
import com.lhq.loader.exception.BaseException;
import com.lhq.loader.function.LngLatTrans;
import com.lhq.loader.service.IStoreService;
import com.lhq.loader.service.LocalStoreService;
import com.lhq.loader.service.SLocalStoreService;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 计算瓦片数量、瓦片地址，放到队列中让下载器下载
 * 
 * @author 灯火-lhq910523@sina.com
 * @time 2020-09-16 13:50:38
 */
public class TestMapServiceToolkit {
    private static final Logger logger = LoggerFactory.getLogger(TestMapServiceToolkit.class);
    public static AtomicInteger count = new AtomicInteger(0);

    private static ExecutorService downTaskPool = null;
    private static Executor executor = null;
    private static int retryTime = 0;
    private static ExecutorService tileDownPool = null;
    private static SLocalStoreService storeService = new SLocalStoreService();
    private static String baseUrl1 = "http://api%s.map.bdimg.com/customimage/tile?&x={x}&y={y}&z={z}&udt=20210506&scale=1&ak=06AXWgRUPX10qacxDjUXHqN20O46qapH&customid=midnight";
    private TestMapServiceToolkit() {
    }


    public static void main(String[] args) throws InterruptedException {

        // 设置下载线程大小
        TestMapServiceToolkit.initDownTaskPool(20);
        // 初始化executor执行器
        TestMapServiceToolkit.initExecuor(5);
        // 启动多个线程处理瓦片下载
        initTileDownPool(200);
        DownloadParamVO downloadParamVO = new DownloadParamVO();
        downloadParamVO.setId("abcd");
        downloadParamVO.setPath("D:\\gongan\\2\\bmap");
        downloadParamVO.setType("bmap");
        LngLat nortwest = new LngLat();
        nortwest.setLat(45.261478);
        nortwest.setLng(116.367892);
        LngLat southeast = new LngLat();
        southeast.setLat(41.295356);
        southeast.setLng(120.994909);
        downloadParamVO.setNorthwest(nortwest);
        downloadParamVO.setSoutheast(southeast);
        Integer[] zooms = {19};
        downloadParamVO.setZooms(zooms);
        List<DownFile> files = TestMapServiceToolkit.addDownTask(downloadParamVO, (LngLat lngLat, int zoom, Tile tile) -> CoordinateToolkit.bd09_LngLat_To_Tile(lngLat, zoom, tile));
        long total = TestMapServiceToolkit.calcTileCount(downloadParamVO, (LngLat lngLat, int zoom, Tile tile) -> CoordinateToolkit.bd09_LngLat_To_Tile(lngLat, zoom, tile));
        List<List<DownFile>> partition = Lists.partition(files, 1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    System.out.println("公共下载运行加载数目："+total +" 已经下载数据"+count+"已经下了" + ( count.get() / total));
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                }
            }
        }).start();

        for (List<DownFile> pFiles : partition) {
            tileDownPool.execute(() -> TestMapServiceToolkit.downTile(storeService,pFiles));
        }
        Thread.currentThread().join();
    }

    private static synchronized void initTileDownPool(int tilePoolSize) {
        if (tileDownPool == null) {
            tileDownPool = Executors.newFixedThreadPool(tilePoolSize);
        }
    }
    /**
     * 计算下载的瓦片数量
     * 
     */
    public static long calcTileCount(DownloadParamVO downloadParamVO, LngLatTrans transToolkit) {
        // 提前创建对象，重复利用，下面的循环次数可能到达百万次甚至千万次，每次创建对象效率太低
        Tile tile1 = new Tile();
        Tile tile2 = new Tile();
        LngLat northwest = downloadParamVO.getNorthwest();
        LngLat southeast = downloadParamVO.getSoutheast();
        Integer[] zooms = downloadParamVO.getZooms();
        long allSun = 0;
        for (int zoom : zooms) {
            // 经纬度转瓦片
            transToolkit.trans(northwest, zoom, tile1);
            transToolkit.trans(southeast, zoom, tile2);
            int minX = tile1.getX() < tile2.getX() ? tile1.getX() : tile2.getX();
            int minY = tile1.getY() < tile2.getY() ? tile1.getY() : tile2.getY();
            int maxX = tile1.getX() > tile2.getX() ? tile1.getX() : tile2.getX();
            int maxY = tile1.getY() > tile2.getY() ? tile1.getY() : tile2.getY();
            allSun += (maxX - minX + 1) * (maxY - minY + 1);
        }
        return allSun;
    }

    /**
     * 添加下载任务，计算瓦片地址，放入队列中等待下载器下载
     * 
     */
    public static  List<DownFile>  addDownTask(DownloadParamVO downloadParamVO, LngLatTrans transToolkit) {
        List<DownFile> tileQueue = Lists.newArrayList();
        Random r = new Random();
        String baseUrl2 = baseUrl1;
        // 提前创建对象，重复利用，下面的循环次数可能到达百万次甚至千万次，每次创建对象效率太低
        Tile tile1 = new Tile();
        Tile tile2 = new Tile();

        LngLat northwest = downloadParamVO.getNorthwest();
        LngLat southeast = downloadParamVO.getSoutheast();
        Integer[] zooms = downloadParamVO.getZooms();
        String path = downloadParamVO.getPath();

        int minX = 0;
        int minY = 0;
        int maxX = 0;
        int maxY = 0;
        File folder = null;
        String fileName;
        String url;
        StringBuilder sb = new StringBuilder();

        boolean useMongoStore = false;
        outer: for (int zoom : zooms) {
            // 经纬度转瓦片
            transToolkit.trans(northwest, zoom, tile1);
            transToolkit.trans(southeast, zoom, tile2);

            minX = tile1.getX() < tile2.getX() ? tile1.getX() : tile2.getX();
            minY = tile1.getY() < tile2.getY() ? tile1.getY() : tile2.getY();
            maxX = tile1.getX() > tile2.getX() ? tile1.getX() : tile2.getX();
            maxY = tile1.getY() > tile2.getY() ? tile1.getY() : tile2.getY();

            for (int x = minX; x <= maxX; x++) {
                sb.setLength(0);
                if (!useMongoStore) {
                    sb.append(path).append(File.separator).append(zoom).append(File.separator).append(x);
                    folder = new File(sb.toString());
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                }

                for (int y = minY; y <= maxY; y++) {

                    // 允许下载
                    sb.setLength(0);
                    if (!useMongoStore) {
                        fileName = sb.append(folder.getPath()).append(File.separator).append(y).append(".png").toString();
                    } else {
                        fileName = sb.append(zoom).append("_").append(x).append("_").append(y).append(".png").toString();
                    }
                    baseUrl2 = String.format(baseUrl2,r.nextInt(3));
                    url = baseUrl2.replace("{x}", String.valueOf(x)).replace("{y}", String.valueOf(y)).replace("{z}", String.valueOf(zoom));
                    try {
                        tileQueue.add(new DownFile(url, fileName, downloadParamVO.getType(), downloadParamVO.getId()));
                    } catch (Exception e) {
                        logger.error(e.getLocalizedMessage(), e);
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
        return tileQueue;
    }

    /**
     * 下载瓦片
     * 
     * @param storeService
     */
    public static void downTile(IStoreService storeService,List<DownFile> files) {
            try {
                for(DownFile downFile : files){
                    try {
                        if (!storeService.exsits(downFile.getFileName(), downFile.getMapType())) {
                            //logger.info("{}下载失败:{}", downFile.getFileName(), downFile.getUrl());
                            byte[] content = executor.execute(Request.Get(downFile.getUrl()).connectTimeout(5000).socketTimeout(5000)).returnContent().asBytes();
                            int i = 0;
                            while(!IsValidPic(content)){
                                if (i==retryTime) {
                                    break;
                                }
                                content = executor.execute(Request.Get(downFile.getUrl()).connectTimeout(5000).socketTimeout(5000)).returnContent().asBytes();
                                i++;
                            }
                            if (IsValidPic(content)) {
                                storeService.store(downFile.getFileName(), downFile.getMapType(), content);
                            }
                        }
                    } catch (Exception e) {
                        logger.info("{}下载失败:{}", downFile.getFileName(), downFile.getUrl());
                        logger.error(e.getMessage(), e);
                    }
                    count.getAndIncrement();
                }
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage(), e);
                Thread.currentThread().interrupt();
            }

    }
    static boolean IsValidPic( byte [] bts)
    {
        if (bts == null || bts.length < 4 )
        {
            return false ;
        }
        String fileClass = "" ;
        int len = bts.length;
        try
        {
            fileClass = String.valueOf(bts[0]);
            fileClass += String.valueOf(bts[ 1 ]);
            fileClass = fileClass.trim();
            if (fileClass == " 7173 " || fileClass == " 13780 " ) // 7173是gif;,13780是PNG;
            {
                return true ;
            }
            else // JpgOrJpeg
            {
                byte [] Jpg = new byte [ 4 ];

                Jpg[0] = (byte) 0xff;

                Jpg[ 1 ] = (byte) 0xd8;

                Jpg[ 2 ] = (byte) 0xff;

                Jpg[ 3 ] = (byte) 0xd9;

                if (bts[ 0 ] == Jpg[ 0 ] && bts[ 1 ] == Jpg[ 1 ]
                        && bts[len - 2 ] == Jpg[ 2 ] && bts[len - 1 ] == Jpg[ 3 ])
                {
                    return true ;
                }
                else
                {
                    return false ;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return false ;
        }
    }

    /**
     * 初始化线程池
     * @param downPoolSize
     */
    public static synchronized void initDownTaskPool(int downPoolSize) {
        if (downTaskPool == null) {
            downTaskPool = new ThreadPoolExecutor(downPoolSize, downPoolSize, 30, TimeUnit.SECONDS, 
                    new LinkedBlockingQueue<>(50),
                    new RejectedExecutionHandler() {
                        @Override
                        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                            throw new BaseException("下载任务已满，请等其他下载任务结束或删除其他下载任务");
                        }
                    });
        }
    }

    /**
     * 初始化executor执行器
     * 
     * @param retryNum
     */
    public static synchronized void initExecuor(int retryNum) {
        retryTime = retryNum;
        if(executor == null) {
            executor = Executor.newInstance(
                    HttpClientBuilder.create()
                        .setRetryHandler((exception, executionCount, context) -> {
                            return executionCount < retryNum;
                        })
                        .build());
        }
    }
    
}
