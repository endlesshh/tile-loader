import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class ReyTry {
    private static Executor executor = null;
    static {
        if(executor == null) {
            executor = Executor.newInstance(
                    HttpClientBuilder.create()
                            .setRetryHandler((exception, executionCount, context) -> {
                                System.out.println("=============");
                                System.out.println(context);
                                return executionCount < 3;
                            })
                            .build());
        }
    }
    public static void main(String[] args) throws IOException {
        //String url = "http://maponline0.bdimg.com/tile/?qt=vtile&x=3195&y=1230&z=14&styles=pl&scaler=1&udt=20210506&from=jsapi3_0";
        //String url = "http://api2.map.bdimg.com/customimage/tile?&x=341&y=129&z=11&udt=20210506&scale=1&ak=TvYKOGMeqBpXPCSqAciESN1p1xbtwmvI&customid=dark";
        String url = "http://www.baidu.com";

        System.out.println("---------------------");
        String str=String.format(url, 1);
        System.out.println(str);
        byte[] content = executor.execute(Request.Get(url).connectTimeout(5000).socketTimeout(5000)).returnContent().asBytes();
        System.out.println(getPicType(content));

    }
    public static boolean getPicType(byte[] b) {
        //读取文件的前几个字节来判断图片格式
        String type = byte2hex(b).toUpperCase();
        if (type.contains("FFD8FF")) {
            return true;//"jpg";
        } else if (type.contains("89504E47")) {
            return true;//"png";
        } else if (type.contains("47494638")) {
            return true;//"gif";
        } else if (type.contains("424D")) {
            return true;//"bmp";
        } else {
            return false;
        }
    }
    public static String byte2hex(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int i = 0; i < b.length; i++) {
            stmp = Integer.toHexString(b[i] & 0xFF).toUpperCase();
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
        }
        return hs.toString();
    }

}
