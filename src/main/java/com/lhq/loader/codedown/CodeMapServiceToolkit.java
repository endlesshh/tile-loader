package com.lhq.loader.codedown;

import com.google.common.collect.Lists;
import com.lhq.loader.bean.DownFile;
import com.lhq.loader.bean.LngLat;
import com.lhq.loader.bean.Tile;
import com.lhq.loader.commons.toolkit.CoordinateToolkit;
import com.lhq.loader.controller.vo.DownloadParamVO;
import com.lhq.loader.exception.BaseException;
import com.lhq.loader.function.LngLatTrans;
import com.lhq.loader.service.IStoreService;
import com.lhq.loader.service.MapServiceToolkit;
import com.lhq.loader.service.SLocalStoreService;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 计算瓦片数量、瓦片地址，放到队列中让下载器下载
 * 
 * @author 灯火-lhq910523@sina.com
 * @time 2020-09-16 13:50:38
 */
public class CodeMapServiceToolkit {
    private static final Logger logger = LoggerFactory.getLogger(CodeMapServiceToolkit.class);
    public static AtomicInteger count = new AtomicInteger(0);
    private static ArrayBlockingQueue<List<DownFile>> tileQueue;
    private static ExecutorService downTaskPool = null;
    private static List<Executor> executors = null;
    private static ExecutorService tileDownPool = null;
    private static SLocalStoreService storeService = new SLocalStoreService();
    //个性化地图
    //private static String baseUrl1 = "http://api%s.map.bdimg.com/customimage/tile?&x=%s&y=%s&z=%s&udt=20210506&scale=1&ak=06AXWgRUPX10qacxDjUXHqN20O46qapH&customid=midnight";
    //最新地图
    private static String baseUrl1 = "http://maponline%s.bdimg.com/tile/?qt=vtile&x=%s&y=%s&z=%s&styles=pl&scaler=1&udt=20210506&from=jsapi3_0";
    private CodeMapServiceToolkit() {
    }

    private static int tilePoolSize = 250;
    private static int downPoolSize = 30;
    private static int retryNum = 5;
    //public static void main(String[] args) throws InterruptedException {
    public static void run() {
        CodeMapServiceToolkit.initTileQueue(tilePoolSize * 3);
        // 设置下载线程大小
        CodeMapServiceToolkit.initDownTaskPool(downPoolSize);
        // 初始化executor执行器
        CodeMapServiceToolkit.initExecuor(retryNum);
        // 启动多个线程处理瓦片下载
        initTileDownPool(tilePoolSize);
        DownloadParamVO downloadParamVO = new DownloadParamVO();
        downloadParamVO.setId("111");
        downloadParamVO.setPath("F:\\huanbao\\bdmapnew\\bmap");
        downloadParamVO.setType("bmap");
        LngLat nortwest = new LngLat();
        nortwest.setLat(45.261478);
        nortwest.setLng(116.367892);
        LngLat southeast = new LngLat();
        southeast.setLat(41.295356);
        southeast.setLng(120.994909);
        downloadParamVO.setNorthwest(nortwest);
        downloadParamVO.setSoutheast(southeast);
        Integer[] zooms = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
        downloadParamVO.setZooms(zooms);
        CodeMapServiceToolkit.addDownTask(downloadParamVO, (LngLat lngLat, int zoom, Tile tile) -> CoordinateToolkit.bd09_LngLat_To_Tile(lngLat, zoom, tile));
        long total = CodeMapServiceToolkit.calcTileCount(downloadParamVO, (LngLat lngLat, int zoom, Tile tile) -> CoordinateToolkit.bd09_LngLat_To_Tile(lngLat, zoom, tile));
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                BigDecimal bigDecimal = new BigDecimal(count.get());
                BigDecimal bigDecimal1 = new BigDecimal(total);
                System.out.println(new Date() +"总共下载瓦片数目："+total +" 已经下载数目"+count+"已经下了" + (bigDecimal.divide(bigDecimal1,10,BigDecimal.ROUND_HALF_UP))+"%");
                /*
                 * List<Job> jobs = mapper.JobListNotDone(); for(Job job : jobs) { Task task =
                 * new Task(job.getUuid()); tp.IssueOperation(task); }
                 */
            }
        },1000,10000);
        for (int i = 0; i < tilePoolSize; i++) {
            tileDownPool.execute(() -> CodeMapServiceToolkit.downTile(storeService,retryNum));
        }
        //Thread.currentThread().join();
    }

    private static synchronized void initTileDownPool(int tilePoolSize) {
        if (tileDownPool == null) {
            tileDownPool = Executors.newFixedThreadPool(tilePoolSize);
        }
    }
    /**
     * 计算下载的瓦片数量
     * 
     */
    public static long calcTileCount(DownloadParamVO downloadParamVO, LngLatTrans transToolkit) {
        // 提前创建对象，重复利用，下面的循环次数可能到达百万次甚至千万次，每次创建对象效率太低
        Tile tile1 = new Tile();
        Tile tile2 = new Tile();
        LngLat northwest = downloadParamVO.getNorthwest();
        LngLat southeast = downloadParamVO.getSoutheast();
        Integer[] zooms = downloadParamVO.getZooms();
        long allSun = 0;
        for (int zoom : zooms) {
            // 经纬度转瓦片
            transToolkit.trans(northwest, zoom, tile1);
            transToolkit.trans(southeast, zoom, tile2);
            int minX = tile1.getX() < tile2.getX() ? tile1.getX() : tile2.getX();
            int minY = tile1.getY() < tile2.getY() ? tile1.getY() : tile2.getY();
            int maxX = tile1.getX() > tile2.getX() ? tile1.getX() : tile2.getX();
            int maxY = tile1.getY() > tile2.getY() ? tile1.getY() : tile2.getY();
            allSun += (maxX - minX + 1) * (maxY - minY + 1);
        }
        return allSun;
    }

    /**
     * 添加下载任务，计算瓦片地址，放入队列中等待下载器下载
     *synchronized
     */
    public static  void addDownTask(DownloadParamVO downloadParamVO, LngLatTrans transToolkit) {

        downTaskPool.execute(() -> {
            Random r = new Random();
            String baseUrl2 = baseUrl1;
            // 提前创建对象，重复利用，下面的循环次数可能到达百万次甚至千万次，每次创建对象效率太低
            Tile tile1 = new Tile();
            Tile tile2 = new Tile();

            LngLat northwest = downloadParamVO.getNorthwest();
            LngLat southeast = downloadParamVO.getSoutheast();
            Integer[] zooms = downloadParamVO.getZooms();
            String path = downloadParamVO.getPath();

            int minX = 0;
            int minY = 0;
            int maxX = 0;
            int maxY = 0;
            File folder = null;
            String fileName;
            String url;
            StringBuilder sb = new StringBuilder();

            boolean useMongoStore = false;
            outer: for (int zoom : zooms) {
                // 经纬度转瓦片
                transToolkit.trans(northwest, zoom, tile1);
                transToolkit.trans(southeast, zoom, tile2);

                minX = tile1.getX() < tile2.getX() ? tile1.getX() : tile2.getX();
                minY = tile1.getY() < tile2.getY() ? tile1.getY() : tile2.getY();
                maxX = tile1.getX() > tile2.getX() ? tile1.getX() : tile2.getX();
                maxY = tile1.getY() > tile2.getY() ? tile1.getY() : tile2.getY();

                for (int x = minX; x <= maxX; x++) {
                    sb.setLength(0);
                    if (!useMongoStore) {
                        sb.append(path).append(File.separator).append(zoom).append(File.separator).append(x);
                        folder = new File(sb.toString());
                        if (!folder.exists()) {
                            folder.mkdirs();
                        }
                    }
                    List<DownFile> tileQueueLists = Lists.newArrayList();
                    for (int y = minY; y <= maxY; y++) {
                        // 允许下载
                        sb.setLength(0);
                        if (!useMongoStore) {
                            fileName = sb.append(folder.getPath()).append(File.separator).append(y).append(".png").toString();
                        } else {
                            fileName = sb.append(zoom).append("_").append(x).append("_").append(y).append(".png").toString();
                        }
                        url = String.format(baseUrl2,r.nextInt(4),String.valueOf(x),String.valueOf(y),String.valueOf(zoom));
                        //url = String.format(baseUrl2,1,String.valueOf(x),String.valueOf(y),String.valueOf(zoom));
                        // url = baseUrl2.replace("{x}", String.valueOf(x)).replace("{y}", String.valueOf(y)).replace("{z}", String.valueOf(zoom));
                        tileQueueLists.add(new DownFile(url, fileName, downloadParamVO.getType(), downloadParamVO.getId()));
                    }
                    try {
                       /* List<List<DownFile>> partition = Lists.partition(tileQueueLists, 100);
                        for(List<DownFile> tiles : partition){
                            tileQueue.put(tiles);
                        }*/
                        tileQueue.put(tileQueueLists);
                    } catch (InterruptedException e) {
                        logger.error(e.getLocalizedMessage(), e);
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
    }

    /**
     * 下载瓦片
     * 
     * @param storeService
     */
    private static  Random random = new Random();
    public static void downTile(IStoreService storeService,int retryNum) {
        //一个线程一个excutor
        Executor executor = executors.get(random.nextInt(retryNum));

        while (true) {
            try {
                List<DownFile> files = tileQueue.take();
                for(DownFile downFile : files){
                    try {
                        if (!storeService.exsits(downFile.getFileName(), downFile.getMapType())) {
                            //logger.info("{} 下载:{}", downFile.getFileName(), downFile.getUrl());
                            byte[] content = executor.execute(Request.Get(downFile.getUrl()).connectTimeout(5000).socketTimeout(5000)).returnContent().asBytes();
                            int i = 0;
                            while(!IsValidPic(content)){
                                if (i==retryNum) {
                                    break;
                                }
                                content = executor.execute(Request.Get(downFile.getUrl()).connectTimeout(5000).socketTimeout(5000)).returnContent().asBytes();
                                i++;
                            }
                            if (IsValidPic(content)) {
                                storeService.store(downFile.getFileName(), downFile.getMapType(), content);
                            }
                        }
                    } catch (Exception e) {
                        logger.info("{}下载失败:{}", downFile.getFileName(), downFile.getUrl());
                        logger.error(e.getMessage(), e);
                    }
                    count.getAndIncrement();
                }
            } catch (InterruptedException e) {
                logger.error(e.getLocalizedMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
    }
    public static boolean IsValidPic(byte[] b) {
        //读取文件的前几个字节来判断图片格式
        String type = byte2hex(b).toUpperCase();
        if (type.contains("FFD8FF")) {
            return true;//"jpg";
        } else if (type.contains("89504E47")) {
            return true;//"png";
        } else if (type.contains("47494638")) {
            return true;//"gif";
        } else if (type.contains("424D")) {
            return true;//"bmp";
        } else {
            return false;
        }
    }
    public static String byte2hex(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int i = 0; i < b.length; i++) {
            stmp = Integer.toHexString(b[i] & 0xFF).toUpperCase();
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
        }
        return hs.toString();
    }

    /**
     * 初始化线程池
     * @param downPoolSize
     */
    public static synchronized void initDownTaskPool(int downPoolSize) {
        if (downTaskPool == null) {
            downTaskPool = new ThreadPoolExecutor(downPoolSize, downPoolSize, 30, TimeUnit.SECONDS, 
                    new LinkedBlockingQueue<>(50),
                    new RejectedExecutionHandler() {
                        @Override
                        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                            throw new BaseException("下载任务已满，请等其他下载任务结束或删除其他下载任务");
                        }
                    });
        }
    }

    /**
     * 初始化executor执行器
     *
     * @param retryNum
     */
    public static synchronized void initExecuor(int retryNum) {
        executors = Lists.newArrayList();
        for(int i=0;i<retryNum;i++){
            executors.add(Executor.newInstance(
                    HttpClientBuilder.create()
                            .setRetryHandler((exception, executionCount, context) -> {
                                return executionCount < retryNum;
                            })
                            .build())) ;
        }
    }
    public static synchronized void initTileQueue(int capacity) {
        if(tileQueue == null) {
            tileQueue = new ArrayBlockingQueue<>(capacity);
        }
    }
}
